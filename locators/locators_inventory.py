'''Inventory page related locator'''
from locators.locators_common import locators

class Inventory_locators(locators):
     
    def __init__(self, url):
        '''__init__'''
        self.url = url
        locators.locDict['inventory menu'] = '//*[@id="body_body_content_ucSideNav_divInventory"]'
        #inventory_menu = '//*[@id="body_body_content_ucSideNav_divMainWrapper"]'
        locators.locDict['create listing menu'] = '//*[@id="body_body_content_ucSideNav_liCreateAds"]'
        locators.locDict['proton listing'] ='//div[@class="make-group"]/div/a/span/img[1]'
        locators.locDict['proton saga'] = '//*[@class="thm-btn-group thm-btn-group-default"]/a[9]'
        locators.locDict['proton 2015'] = '//*[@class="thm-btn thm-btn-default thm-btn-action thm-btn-action-large"][3]'
        #proton_2015 = proton_saga + '/a[3]'
        locators.locDict['automatic'] = '//*[@id="body_body_content_lnkBtnTranAutomatic"]'
        locators.locDict['select your car'] = '//*[@id="body_body_content_rptVehicleResult_lnkVehicle_0"]'
        locators.locDict['used details'] = '//*[@id="usedAnchor"]'
        locators.locDict['cbu details'] = '//*[@id="cbu"]'
        locators.locDict['location dropdown'] = '//*[@id="ddlStates"]'
      
        locators.locDict['location details'] = '//*[@id="ddlStates"]/option[13]'
        locators.locDict['mileage details'] = '//*[@id="body_body_content_pnlMileage"]/div[1]/div[1]'
        locators.locDict['mileage 25km details'] = '//*[@id="body_body_content_rptMileageRange1_btnMileage1_4"]'
        locators.locDict['colour gold details'] = '//*[@id="body_body_content_rptColours_lnkColours_10"]'
        locators.locDict['plate number details'] = '//*[@id="txtPlateNumber"]'
        locators.locDict['reference number details'] = '//*[@id="txtReferenceNum"]'
        locators.locDict['price details'] = '//*[@id="txtPrice"]'    
        locators.locDict['select from selling points details'] = '//label[@for="contact_method"][1]'
        locators.locDict['insert from selling points details'] = '//*[@id="AdDescriptionTextarea"]'
        locators.locDict['next button'] = '//*[@id="lnkNext"]'
        locators.locDict['add files button'] = '//*[@class="plupload_buttons"]' 
        locators.locDict['preview ad button'] = '//*[@id="lnkBtnNext"]'         
        locators.locDict['upload photo visible'] = '//div[contains(@class,"image-item")]'            
        
                
