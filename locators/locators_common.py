import re
from selenium.webdriver.support.ui import Select

'''Class about All locators'''
class locators(object):
    
    locDict = {}
    # Dealer partal related locators
    ##############################################################################
    locDict['delete save car button'] = '(//div[contains(@id, "pnlSavedCar")]/div/div[@class="invcard-manage"]/div/div/a[contains(@id, "SavedCar_lnkDelete")])'
    locDict['view save car button'] = '(//div[contains(@id, "pnlSavedCar")]/div/div[@class="invcard-manage"]/div/div/a[contains(@id, "SavedCar_lnkView")])'
    
    # home page related locators
    ##############################################################################
    # header
    locDict['sign in link'] = {'my':'//*[@id="body_lnkBtnSignIn"]', 'id': '//div[contains(@class, "desk-three-tenths")]/ul/li/a[text()="Masuk"]'}
    locDict['username textbox'] = '//*[@id="txtLoginEmail"]'
    #username_textbox = '//div[@class="control-group"]/input[@id="txtLoginEmail"]'
    locDict['password textbox'] = '//*[@id="txtLoginPassword"]'
    #password_textbox = '//div[@class="form-group"]/input[@id="password_login"]'
    locDict['sign in button'] = '//*[@id="btnSignIn"]'

    #sign_in_button = '//div[@class="form-action"]/button'
    
    locDict['dashboard text']= '//*[@id="main"]/div[6]/div[1]/div[1]/div[1]/div[1]/div'
    locDict['verified label'] ='//*[@id="body_body_content_content_ucProfileTimeline_pnlIsVerified"]'    
    locDict['ad inventory chart'] ='//*[@id="body_body_content_content_UpdatePanel1"]/div/div[2]' 

    locDict['home image link'] = '//header/div/div/div/a/img'
    
  
    # profile menu
    locDict['profile menu'] = '//*[@id="body_body_content_ucSideNav_divProfile"]'
    locDict['change password'] ='//*[@id="body_body_content_content_lnkChangePassword"]'
    locDict['ignore update'] ='//*[@id="buorgig"]'
    locDict['profile current password textbox'] = '//*[@id="txtPassword"]'
    locDict['profile new password textbox'] ='//*[@id="txtNewPassword"]'
    locDict['profile confirm password textbox'] = '//*[@id="txtConfirmNewPassword"]'
    locDict['submit password button'] ='//*[@id="btnSubmit"]'
    #password_successfully_changed ='//*[contains(@id,"Password Successfully Changed")]'
    locDict['password successfully changed'] = '//*[@id="body_body_content_content_divSuccess"]'
    locDict['email textbox'] = '//*[@id="txtEmail"]'
    locDict['mobile number textbox'] = '//*[@id="txtMobile"]'
    locDict['whatsapp textbox'] = '//*[@id="txtWhatapps"]'
    locDict['update button'] = '//*[@id="btnUpdate"]'
    locDict['profile successfully updated'] = '//*[@id="body_body_content_content_lblUpdateSuccessfully"]'                    

    
#



    locDict['profile home'] = locDict['profile menu'] + '/../ul/li/a[contains(@class, "home")]'
    locDict['profile profile'] = locDict['profile menu'] + '/../ul/li/a[contains(@class, "profile")]'
    locDict['profile saved cars'] = locDict['profile menu'] + '/../ul/li/a[contains(@class, "saved-cars")]'
    locDict['profile logout'] = locDict['profile menu'] + '/../ul/li/a[contains(@class, "logout")]'
    
    # footer section
    locDict['common bp footer section'] = '(//div[contains(@class, "one-third")]/ul[contains(@class, "nav--footer")])'
    # unused for dictionary search
    help_and_FAQ_section = '%s[6]/li' % (locDict['common bp footer section'],)
    
    # Help and FAQ
    locDict['contact us link'] = {'my': help_and_FAQ_section + '/a[text()="Contact us"]',
                       'id': help_and_FAQ_section + '/a[text()="Hubungi kami"]'}
    locDict['trusted dealer link'] = help_and_FAQ_section + '/a[text()="Buying from trusted dealers"]'
    locDict['buying a car link'] = help_and_FAQ_section + '/a[text()="Buying a car"]'
    locDict['selling a car link'] = help_and_FAQ_section + '/a[text()="Selling a car"]'
    locDict['faq link'] = help_and_FAQ_section + '/a[text()="FAQ"]'
    locDict['ad guide link'] = help_and_FAQ_section + '/a[text()="Ad Guidelines"]'
    locDict['privacy policy link'] = help_and_FAQ_section + '/a[text()="Privacy policy"]'
    locDict['panduan beriklanan link'] = help_and_FAQ_section + '/a[text()="Panduan Beriklan"]'
    locDict['kebijakan privasi link'] = help_and_FAQ_section + '/a[text()="Kebijakan privasi"]'
    
    # loading icon
    locDict['loading icon'] = '//div[@class="loading-screen"]'

    # Error page
    ##############################################################################
    locDict['error page'] = '//div[contains(@class, "error-page")]'


    # Functions
    ##############################################################################
    def get_country_of_portal(self):
        server_path = re.split('\.', self.url)
        portal = server_path[-2]
        country = ''
        
        if re.search('carlist', portal, re.IGNORECASE):
            country = 'my'
        elif re.search('mobil123', portal, re.IGNORECASE):
            country = 'id'
            
        return country
    
    def get_locator(self, element, num=None, text=None):
        '''return locator object'''
        element = element.strip()
        
        if text is None:
            element = element.lower()
        
        locator = self.locDict[element]
    
        element = re.sub('_', ' ', element)
     
        if type(locator) is not type(str()):
            locator = locator[self.get_country_of_portal()]

        if num is not None:
            locator = locator + '[%d]' % (num,)

        if text is not None:
            if not (re.search('text()', locator) or re.search('data-value', locator)):
                locator = locator + '[@data-value="%s"]'

            locator = locator % (text,)

        return locator
