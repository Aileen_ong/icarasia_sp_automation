'''Inventory page related locator'''
from locators.locators_common import locators

class Crm_locators(locators):
     
    def __init__(self, url):
        '''__init__'''
        self.url = url
        locators.locDict['crm login button'] = '//*[@class="btn btn-primary btn-block btn-flat"]'
        locators.locDict['crm username textbox'] = '//*[@id="UserName"]'
        locators.locDict['crm password textbox'] = '//*[@id="Password"]'
        locators.locDict['dashboard to CRM Portal'] = '//*[contains(@class, "content-header")]'
        #bulk upload locator
        locators.locDict['bulk upload tool treeview'] = '//i[contains(@class,"upload")]'        
        locators.locDict['bulk upload listings'] = '//a[contains(@href,"BulkUpload")]'     
        locators.locDict['bulk upload username button'] = '//*[@id="btnUsername_1"]'
        locators.locDict['bulk upload add username button'] = '//*[@id="btnAddUsername"]'        
        locators.locDict['bulk upload username2 button'] = '//*[@id="btnUsername_2"]'         
        locators.locDict['bulk upload search username'] = '//*[contains(@placeholder,"Search by Username or Contact Name")]'                              
        locators.locDict['bulk upload go button'] = '//button[contains(@name,"FilterCustomer")]'           
        locators.locDict['bulk upload search datausername'] = '//*[@id="rdContactId"]'           
        locators.locDict['bulk upload choose button'] = '//button[contains(@class,"btn btn-primary btnChoose")]'                   
        locators.locDict['bulk upload add listings button'] = '//*[@id="btnAddField"]'            
        locators.locDict['bulk upload select main image'] = '//*[@id="dropzoneForm1"]'   
        locators.locDict['bulk upload select secondary image'] = '//*[@id="dropzoneForm2"]'
        locators.locDict['bulk upload expand details'] = '//i[(@class="fa fa-plus")][1]'                  
        locators.locDict['bulk upload vehicle dropdown'] = '//*[@id="Vehicle"]'
        locators.locDict['bulk upload millagerange dropdown'] = '//*[@id="MillageRange"]'        
        locators.locDict['bulk upload reference number'] = '//*[@id="RefNo"]'                
        locators.locDict['bulk upload plate number'] = '//*[@id="PlateNumber"]'             
        locators.locDict['bulk upload price'] = '//*[@id="Price"]'                   
        locators.locDict['bulk upload listing description'] = '//*[@id="ListingDescription"]'    
        locators.locDict['bulk upload add listing button'] = '//*[@id="ListingAddButton"]'          
        locators.locDict['bulk upload add common description'] = '//*[@id="CommonDescription"]'    
        locators.locDict['bulk upload select all static'] = '//*[@id="StaticSelectAll"]'
        locators.locDict['bulk upload upload button'] = '//*[@value="Upload"]'    
        locators.locDict['bulk upload success message'] = '//div[text()[contains(.,"Your listings have been submitted for uploading.")]]'    
        locators.locDict['bulk upload progress'] = '//a[contains(@href,"BulkUpload/Progress")]'  
        locators.locDict['bulk upload progress grid'] = '//*[@id="grid"]'  
        locators.locDict['bulk upload progress pagination'] = '//a[contains(@href,"BulkUpload/Progress?page=2")]'  
        locators.locDict['bulk upload progress action button'] = '//*[@class="glyphicon glyphicon-search"]'  
        locators.locDict['bulk upload progress details pagination'] = '//a[contains(@href,"page=2")]'  
        locators.locDict['bulk upload progress details back to list button'] = '//span[(text()="Back to List")]'  
        locators.locDict['bulk upload progress title'] = '//h1[(text()="Bulk Upload Progress")]'          
        locators.locDict['bulk upload progress search textfield'] = '//*[contains(@placeholder,"Search by Uploaded By")]'  
        locators.locDict['bulk upload progress go button'] = '//*[(@type="submit")]'  
        #removal tool locator
        locators.locDict['bulk removal tool treeview'] = '//span[(text()="Bulk Removal Tool")]'
        locators.locDict['bulk removal remove listings'] = '//*[(@href="/BulkRemoval")]'          
        locators.locDict['bulk removal browse file'] = '//*[@id="idFileUpload"]'  
        locators.locDict['bulk removal description'] = '//*[@id="idDescription"]'          
        locators.locDict['bulk removal upload button'] = '//*[@id="btnUpload"]'         
        locators.locDict['bulk removal success message'] = '//div[text()[contains(.,"Your request have been submitted.")]]'             
        locators.locDict['bulk removal failed message'] = '//div[text()[contains(.,"Please upload the file in csv format.")]]'   
        locators.locDict['bulk removal progress'] = '//a[contains(@href,"BulkRemoval/Progress")]'
        locators.locDict['bulk removal progress title'] = '//a[contains(@href,"BulkRemoval/Progress")]'                
        locators.locDict['bulk removal request details title'] = '//h1[(text()="Bulk Removal Request Details")]'
        locators.locDict['bulk removal request details back button'] = '//span[(text()="Back to List")]' 
        #contact locator           
        locators.locDict['customer treeview'] = '//span[(text()="Customer")]'    
        locators.locDict['contact'] = '//a[(@href="/Contact")]'       
        locators.locDict['contact username input'] = '//input[(@id="txtUsername")]'   
        locators.locDict['contact search button'] = '//button[(@id="btnSearch")]'     
        locators.locDict['contact search username'] = '//a[(@title="Contact Detail")]'
        locators.locDict['contact result table'] = '//*[@title="Contact Detail"]'
        locators.locDict['contact email input'] = '//*[@id="txtEmailname"]'
        locators.locDict['contact phone number input'] = '//*[@id="txtPhonenumber"]'
        locators.locDict['contact full name input'] = '//*[@id="txtFullname"]'
        locators.locDict['contact result details'] = '//h3[(text()="aileen_dealer_my2")]'
        locators.locDict['contact result action details'] = '//a[@title="Detail"]'
        locators.locDict['contact result action edit'] = '//a[@title="Edit"]'
        locators.locDict['contact edit email textbox'] = '//*[@id="EmailAddress"]'
        locators.locDict['contact edit mobile phone textbox'] = '//input[@id="MobileNo"]'
        locators.locDict['contact save button'] = '//input[@type="submit"]'
        locators.locDict['contact account management tab'] = '//a[contains(@href,"#settings")]'
        
        





