'''Home page related locator'''
from locators.locators_common import locators

class Home_Page_locators(locators):
    
      #change profile picture
    locators.locDict['update profile picture'] ='//*[@id="body_body_content_content_ucProfileTimeline_profilePic"]' 
    locators.locDict['change photo button']= '//*[@id="modal"]/div/div/div[2]/label'
    #input_file = '//*[@id="body_body_content_content_ucProfileTimeline_profilePic"]/div'
    locators.locDict['input file'] = '//*[@id="browsepic"]'    
    locators.locDict['photo zoom in'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[1]/button[1]'
    locators.locDict['photo zoom out'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[1]/button[2]/span'
    locators.locDict['photo move left'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[2]/button[1]'
    locators.locDict['photo move right'] ='//*[@id="modal"]/div/div/div[2]/div[2]/div[2]/button[1]'
    locators.locDict['photo move up'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[2]/button[3]'
    locators.locDict['photo move down'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[2]/button[4]'
    locators.locDict['photo rotate left'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[3]/button[1]/span'
    locators.locDict['photo rotate right'] ='//*[@id="modal"]/div/div/div[2]/div[2]/div[3]/button[2]'
    locators.locDict['photo flip horizontal'] ='//*[@id="modal"]/div/div/div[2]/div[2]/div[4]/button[1]'
    locators.locDict['photo flip vertical'] = '//*[@id="modal"]/div/div/div[2]/div[2]/div[4]/button[2]'
    locators.locDict['photo save button'] ='//*[@id="modal"]/div/div/div[2]/div[2]/div[5]/button[1]'
    
    # header
    header = '//div[contains(@class, "grid")]/div/ul[contains(@class, "header")]'
    kereta_murah_link = {'my': '%s/li/a[contains(text(), "Kereta Murah")]' % (header)}
    cars_for_sales_link = {'my': '%s/li/a[contains(text(), "Cars for sale")]' % (header)}
    new_car_deals_link = {'my': '%s/li/a[contains(text(), "New car deals")]' % (header)}
    research_new_cars_link = {'my': '%s/li/a[contains(text(), "Research new cars")]' % (header)}
    automotive_news_link = {'my': '%s/li/a[contains(text(), "Automotive news")]' % (header)}
    tools_link = {'my': '%s/li/a[contains(text(), "Tools")]' % (header)}
    mobil_bekas_link = {'id': '%s/li/a[contains(text(), "Mobil Bekas")]' % (header)}
    mobil_baru_link = {'id': '%s/li/a[contains(text(), "Mobil Baru")]' % (header)}
    motor_link = {'id': '%s/li/a[contains(text(), "Motor")]' % (header)}
    berita_mobil_link = {'id': '%s/li/a[contains(text(), "Berita Mobil")]' % (header)}
    bantuan_link = {'id': '%s/li/a[contains(text(), "Bantuan")]' % (header)}
    register_link = {'my': '//div[contains(@class, "desk-three-tenths")]/ul/li/a[text()="Register"]',
                     'id': '//div[contains(@class, "desk-three-tenths")]/ul/li/a[text()="Daftar"]'}
    sell_your_car_btn = '//div[contains(@class, "desk-three-tenths")]/ul/li/a[contains(@class, "btn--primary")]'
    
    # footer section
    common_bp_footer_section = '(//div[contains(@class, "one-third")]/ul[contains(@class, "nav--footer")])'
    cars_for_sale_section = {'my': '%s[1]/li' % (common_bp_footer_section,)}
    new_car_search_section = {'my':'%s[2]/li' % (common_bp_footer_section,)}
    sell_your_car_section = {'my':'%s[3]/li' % (common_bp_footer_section,)}
    new_and_review_section = {'my':'%s[4]/li' % (common_bp_footer_section,)}
    site_info_section = {'my':'%s[5]/li' % (common_bp_footer_section,)}
    mobil_bekas_section = {'id': '%s[1]/li' % (common_bp_footer_section,)}
    pencari_mobil_baru_section = {'id':'%s[2]/li' % (common_bp_footer_section,)}
    pasang_iklan_section = {'id':'%s[3]/li' % (common_bp_footer_section,)}
    berita_mobil_section = {'id':'%s[4]/li' % (common_bp_footer_section,)}
    informasi_situs_section = {'id':'%s[5]/li' % (common_bp_footer_section,)}
    
    # footer link within sections
    # Cars for sale
    sale_used_car_link = cars_for_sale_section['my'] + '/a[text()="Used cars for sale"]'
    sale_new_car_link = cars_for_sale_section['my'] + '/a[text()="New cars for sale"]'
    request_car_link = cars_for_sale_section['my'] + '/a[text()="Request a car"]'
    email_alert_link = cars_for_sale_section['my'] + '/a[text()="Email alerts"]'
    # New car search
    help_me_choose_link = new_car_search_section['my'] + '/a[text()="Help me choose"]'
    new_car_deals_link = new_car_search_section['my'] + '/a[text()="New car deals"]'
    car_price_list_link = new_car_search_section['my'] + '/a[text()="New car price list"]'
    car_galleries_link = new_car_search_section['my'] + '/a[text()="New car galleries"]'
    compare_car_link = new_car_search_section['my'] + '/a[text()="Compare new cars"]'
    # Sell your Car
    create_ad_link = sell_your_car_section['my'] + '/a[text()="Create Ad"]'
    manage_ad_link = sell_your_car_section['my'] + '/a[text()="Manage Ad"]'
    car_seller_help = sell_your_car_section['my'] + '/a[text()="Car seller help"]'
    # New and review
    auto_news_link = new_and_review_section['my'] + '/a[text()="Auto news"]'
    car_reviews_link = new_and_review_section['my'] + '/a[text()="Car reviews"]'
    car_tips_link = new_and_review_section['my'] + '/a[text()="Car tips"]'
    editorial_team_link = new_and_review_section['my'] + '/a[text()="Editorial Team"]'
    # Site info
    about_us_link = site_info_section['my'] + '/a[text()="About us"]'
    tnc_for_advertisers_link = site_info_section['my'] + '/a[text()="Terms and conditions for advertisers"]'
    tnc_for_website_use_link = site_info_section['my'] + '/a[text()="Terms and conditions for use of the website"]'
    data_protection_en_link = site_info_section['my'] + '/a[text()="Personal data protection notice"]'
    data_protection_ms_link = site_info_section['my'] + '/a[text()="Personal data protection notice (Bahasa Malaysia)"]'
    # Follow us
    follow_us_download_app_section = '//div[contains(@class, "four-twelfths")]/ul'
    fb_link = follow_us_download_app_section + '/li/a[contains(@class, "facebook")]'
    twitter_link = follow_us_download_app_section + '/li/a[contains(@class, "twitter")]'
    youtube_link = follow_us_download_app_section + '/li/a[contains(@class, "youtube")]'
    google_plus_link = follow_us_download_app_section + '/li/a[contains(@class, "gplus")]'
    # Download our app
    google_play_link = follow_us_download_app_section + '/li[contains(@class, "android")]/a'
    apple_app_store_link = follow_us_download_app_section + '/li[contains(@class, "ios")]/a'
    # iCar Network
    icar_newwork = '//div[contains(@class, "four-twelfths")]/div/div/ul/li'
    carlist_link = '%s/a[contains(text(), "Carlist")]' %(icar_newwork)
    one2car_link = '%s/a[contains(text(), "One2car")]' %(icar_newwork)
    mobil123_link = '%s/a[contains(text(), "Mobil123")]' %(icar_newwork)
    carsales_link = '%s/a[contains(text(), "Carsales")]' %(icar_newwork)
    livelifedrive_link = '%s/a[contains(text(), "Livelifedrive")]' %(icar_newwork)
    autospinn_link = '%s/a[contains(text(), "Autospinn")]' %(icar_newwork)
    otospirit_link = '%s/a[contains(text(), "OtoSpirit")]' %(icar_newwork)
    
    # Mobil Bekas
    mobil_bekas_dijual_link = mobil_bekas_section['id'] + '/a[text()="Mobil bekas dijual"]'
    mobil_baru_dijual_link = mobil_bekas_section['id'] + '/a[text()="Mobil baru dijual"]'
    motor_bekas_dijual_link = mobil_bekas_section['id'] + '/a[text()="Mobil bekas dijual"]'
    motor_baru_dijual_link = mobil_bekas_section['id'] + '/a[text()="Mobil baru dijual"]'
    notifikasi_email_link = mobil_bekas_section['id'] + '/a[text()="Notifikasi Email"]'
    # Pencari Mobil Baru
    mobil_baru_link = pencari_mobil_baru_section['id'] + '/a[text()="Mobil baru"]'
    pencari_mobil_baru_link = pencari_mobil_baru_section['id'] + '/a[text()="Pencari Mobil Baru"]'
    daftar_harga_mobil_baru_link = pencari_mobil_baru_section['id'] + '/a[text()="Daftar harga mobil baru"]'
    komparasi_mobil_baru_link = pencari_mobil_baru_section['id'] + '/a[text()="Komparasi mobil baru"]'
    # Pasang Iklan
    pasang_ilkan_mobil_link = pasang_iklan_section['id'] + '/a[text()="Pasang Iklan Mobil"]'
    pasang_ilkan_motor_link = pasang_iklan_section['id'] + '/a[text()="Pasang Iklan Motor"]'
    kelola_iklan_link = pasang_iklan_section['id'] + '/a[text()="Kelola Iklan"]'
    # Berita Mobil
    berita_and_review_link = berita_mobil_section['id'] + '/a[text()="Berita & Review"]'
    tim_editorial_link = berita_mobil_section['id'] + '/a[text()="Tim Editorial"]'
    # Informasi Situs
    tentang_kami_link = informasi_situs_section['id'] + '/a[text()="Tentang kami"]'
    ketentuan_layanan_link = informasi_situs_section['id'] + '/a[text()="Ketentuan Layanan"]'
    syarat_ketentuan_link = informasi_situs_section['id'] + '/a[text()="Syarat dan Ketentuan Yang Berlaku"]'
    data_perlindungan_pribadi_link = informasi_situs_section['id'] + '/a[text()="Data perlindungan pribadi"]'
    

    def __init__(self, url):
        '''__init__'''
        self.url = url
