import re
import subprocess

import pytest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from unittestzero import Assert

from locators.locators_inventory import Inventory_locators
from pages.common_page import Common_Page
from pages.home_page import Home_Page
from util.password_util import Password_helper


class TestInventory_MY():
    
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_create_ads (self, selenium, base_url):
        '''
        @summary: Create listing
        
        Steps:
        1. Login with correct user name (positive test)
        2. Create positive listing
        '''
        
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Inventory_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password()  
        sp.login_with_user('aileen_dealer_my2', pwd)  
        #sp.scroll_to_bottom_of_the_page()

        #sp.hide_homepage_header() # hack to hide the home page header
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.hover ('inventory menu')
        sp.click_on ('create listing menu')
        sp.click_on ('proton listing')
        sp.click_on ('proton saga')
        sp.click_on ('proton 2015')
        sp.click_on ('automatic')
        sp.click_on ('select your car')
        sp.click_on ('used details')
        sp.click_on ('cbu details')   
        sp.select_drop_down_by_value('location dropdown', 'Putrajaya')        
        sp.click_on ('mileage details')
        sp.click_on ('mileage 25km details')
        sp.click_on ('colour gold details')
        sp.fill_in_textbox("w123x", 'plate number details')
        sp.fill_in_textbox("123456789", 'reference number details')
        sp.fill_in_textbox("28000", 'price details') 
        sp.click_on ('select from selling points details')
        #sp.fill_in_textbox("good price", 'insert_from_selling_points_details')
        sp.click_on ('next button')     
        sp.click_on ('add files button')           
        #call AutoIT
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUploadListing.exe")
        if sp.is_element_visible ('upload photo visible'):     
            sp.click_on ('preview ad button')
        



