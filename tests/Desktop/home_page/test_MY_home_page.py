import pytest
import re
from unittestzero import Assert
from pages.home_page import Home_Page
from pages.common_page import Common_Page
from selenium.webdriver.common.keys import Keys
from util.password_util import Password_helper
import subprocess
from locators.locators_home_page import Home_Page_locators


class TestHomePage_MY():
    
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_login (self, selenium, base_url):
        '''
        @summary: Verify correct username
        
        Steps:
        1. Login with correct user name (positive test)
        '''        
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password()  
        sp.login_with_user('aileen_dealer_my', pwd)  
        #sp.scroll_to_bottom_of_the_page()
        
        #sp.hide_homepage_header() # hack to hide the home page header
        #sp.click_on('sign in button')
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        
    
    @pytest.mark.nondestructive
    @pytest.mark.id   
    def test_wrong_username_login (self, selenium, base_url):
        '''
        @summary: Verify incorrect username
        
        Steps:
        1. Login with incorrect user name (negative test)
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_wrong_username', pwd)  
           
        Assert.false(sp.element_visibility('Dashboard text'), 'Dashboard section should not appeared.')             


    @pytest.mark.nondestructive
    @pytest.mark.id   
    def test_verify_dashboard (self, selenium, base_url):
        '''
        @summary: Verify dashboard
        
        Steps:
        1. Login
        2. Check inventory chart and verified label
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my', pwd)  
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        Assert.true(sp.element_visibility('verified label'), 'Verified label should appeared.')   
        Assert.true(sp.element_visibility('ad inventory chart'), 'Ad inventory chart should appeared.')


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_profile_picture (self, selenium, base_url):
        '''
        @summary: Profile picture
        To do
        Steps:
        1. Login seller portal
        2. Check every profile button functionality- positive test
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my', pwd)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('update profile picture')
        sp.click_on ('change photo button')
        #call AutoIT
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUpload.exe")
        sp.click_on ('photo zoom in')
        sp.click_on ('photo zoom out')
        sp.click_on ('photo move left')       
        sp.click_on ('photo move right')  
        sp.click_on ('photo move up')  
        sp.click_on ('photo move down')  
        sp.click_on ('photo rotate left')  
        sp.click_on ('photo rotate right')  
        sp.click_on ('photo flip horizontal')  
        sp.click_on ('photo flip vertical') 
        sp.click_on ('photo flip vertical')         
        sp.click_on ('photo save button')
        Assert.true(sp.switch_to_alert(), "You are uploading incorrect image with error")



    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_small_profile_picture (self, selenium, base_url):
        '''
        @summary: Profile picture
        To do
        Steps:
        1. Login seller portal
        2. Upload small profile picture- negative test
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my', pwd)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('update profile picture')
        sp.click_on ('change photo button')
        #call AutoIT
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\SmallFileUpload.exe")
        alert_text = sp.switch_to_alert().text 
        #print(alert_text)
        #to do
        Assert.equal("The photo you uploaded is invalid. Your photo size must not be smaller than 10KB and not exceeding 6MB. Please upload again", alert_text, "You are uploading the correct picture without error")


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_big_profile_picture (self, selenium, base_url):
        '''
        @summary: Profile picture
        To do
        Steps:
        1. Login seller portal
        2. Upload big profile picture- negative test
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my2', pwd)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('update profile picture')
        sp.click_on ('change photo button')
        #call AutoIT
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\BigFileUpload.exe")
        alert_text = sp.switch_to_alert().text 
        #print(alert_text)
        Assert.equal("The photo you uploaded is invalid. Your photo size must not be smaller than 10KB and not exceeding 6MB. Please upload again", alert_text, "You are uploading the correct picture without error")
        
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_invalid_profile_picture (self, selenium, base_url):
        '''
        @summary: Profile picture
        To do
        Steps:
        1. Login seller portal
        2. Upload big profile picture- negative test
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my2', pwd)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('update profile picture')
        sp.click_on ('change photo button')
        #call AutoIT
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\InvalidFileUpload.exe")
        alert_text = sp.switch_to_alert().text 
        #print(alert_text)
        Assert.equal("The photo you uploaded is invalid. Your photo size must not be smaller than 10KB and not exceeding 6MB. Please upload again", alert_text, "You are uploading the correct picture without error")
        
    
    @pytest.mark.nondestructive
    @pytest.mark.id 
    def test_profile_change_password (self, selenium, base_url):
        '''
        @summary: Profile picture
        To do
        Steps:
        1. Login seller portal
        2. Change new password        
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my', pwd)
        while sp.is_loading():
            sp.wait_for_seconds(1)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('profile menu')
        
        #sp.click_on ('ignore update')        
        sp.click_on ('change password')
        ph = Password_helper(); 
        #ph.write_password();
        current_password = ph.read_password() #read current password
        sp.fill_in_textbox(current_password, 'profile current password textbox')
        newPassword = ph.gen_password(current_password) #generate password
        sp.fill_in_textbox(newPassword, 'profile new password textbox')        
        sp.fill_in_textbox(newPassword, 'profile confirm password textbox') 
        ph.write_password(newPassword) #write new password
        sp.click_on ('submit password button')
       # Assert.true(sp.is_element_visible(sp.get_object('Password Successfully Changed')))
        Assert.true(sp.element_visibility('Password Successfully Changed'), 'Password Successfully Changed message should appeared.')                    


    @pytest.mark.nondestructive
    @pytest.mark.id 
    def test_profile_revert_password (self, selenium, base_url):
        '''
        @summary: Profile picture
        To do
        Steps:
        1. Login seller portal
        2. revert password        
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my', pwd)
        while sp.is_loading():
            sp.wait_for_seconds(1)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('profile menu')
             
        sp.click_on ('change password')
        ph = Password_helper(); 
        current_password = ph.read_password() #read current password
        sp.fill_in_textbox(current_password, 'profile current password textbox')
        newPassword = ph.gen_password(current_password) #generate password
        sp.fill_in_textbox(newPassword, 'profile new password textbox')        
        sp.fill_in_textbox(newPassword, 'profile confirm password textbox') 
        ph.write_password(newPassword) #write new password
        sp.click_on ('submit password button')
        Assert.true(sp.element_visibility('Password Successfully Changed'), 'Password Successfully Changed message should appeared.')                    


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_update_profile (self, selenium, base_url):
        '''
        @summary: Update profile
        To do
        Steps:
        1. Login seller portal
        2. Update profile- email address and contact number
        '''
        myUrl = 'https://dealer.preprod.carlist.my/'
        locator = Home_Page_locators(myUrl)
        sp = Common_Page(selenium, myUrl, locator)
        sp.go_to_home_page()
        
        sp.click_on('sign in link')
        ph = Password_helper(); 
        pwd = ph.read_password() 
        
        sp.login_with_user('aileen_dealer_my', pwd)
        sp.wait_for_seconds(10)      
        Assert.true(sp.element_visibility('Dashboard text'), 'Dashboard section should appeared.')
        sp.click_on ('profile menu')
        sp.clear_value_in_textbox('email textbox')
        sp.fill_in_textbox("aileen.ong@gmail.com", 'email textbox')   
        sp.clear_value_in_textbox('mobile number textbox')   
        sp.fill_in_textbox("0122114929", 'mobile number textbox')
        sp.clear_value_in_textbox('whatsapp textbox')   
        sp.fill_in_textbox("0122114928", 'whatsapp textbox')       
        sp.click_on ('update button')
        Assert.true(sp.element_visibility('profile successfully updated'), 'Profile successfully updated.')            
        