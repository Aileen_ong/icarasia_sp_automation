import subprocess
import urllib2
import pytest
from unittestzero import Assert

from locators.locators_crm import Crm_locators
from pages.common_page import Common_Page

#@pytest.fixture
#def base_url(crmUrl):
#    return'http://crm.preprod.carlist.my:90/Account/login'
    
class TestCRM_MY():

    
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_listing (self, selenium, base_url):
        '''
        @summary: Upload bulk upload listing
        
        Steps: pass
        1. Select correct username
        2. Upload 1 listing (positive test)
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        crm.click_on('bulk upload username button')    
        crm.fill_in_textbox("aileen_dealer_my2", 'bulk upload search username')
        crm.click_on('bulk upload go button ')         
        crm.click_on('bulk upload search datausername')          
        crm.click_on('bulk upload choose button')             
        crm.click_on('bulk upload add listings button') 
        #Add image
        crm.click_on('bulk upload select main image')           
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\MasterUploadListing.exe")
        crm.click_on('bulk upload select secondary image') 
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUploadListing.exe")        
        #epand vehicle
        crm.click_on('bulk upload expand details')                        
        crm.select_drop_down_using_visible_text("Make", "Proton")
        crm.wait_for_seconds(1)   
        crm.select_drop_down_using_visible_text("Model", "Saga")
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Year", "2015")       
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Transmission", "Automatic")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Variant", "1.3 FLX Executive")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload vehicle dropdown', 'PROT15AL')
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("CarType", "USED")        
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("AssembleType", "CKD")           
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Color", "Gold")         
        crm.wait_for_seconds(1)          
        crm.select_drop_down_using_visible_text("Location", "Kuala Lumpur")             
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Area", "Bangsar")     
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("MillageRange", "Other")    
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload millagerange dropdown', '12500')
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("abc123", 'bulk upload reference number')         
        crm.fill_in_textbox("w123x", 'bulk upload plate number') 
        #expand price
        crm.click_on('bulk upload expand details')  
        crm.fill_in_textbox("27000", 'bulk upload price')         
        #expand title & description
        crm.click_on('bulk upload expand details')
        crm.wait_for_seconds(1)          
        crm.fill_in_textbox("testing", 'bulk upload listing description')
        crm.click_on('bulk upload select all static')
        crm.wait_for_seconds(1)      
        crm.click_on('bulk upload add listing button')
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("test bulk upload tool", 'bulk upload add common description')
        crm.wait_for_seconds(1)              
        #successful upload listing           
        crm.click_on('bulk upload upload button')             
        Assert.true(crm.element_visibility('bulk upload success message'), 'Successfully upload listing.')

    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_listing_incorrect_username (self, selenium, base_url):
        '''
        @summary: Choose incorect username and get warning
        
        Steps: pass
        1. Choose incorect username
        2. Warning user to enter correct user name
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        crm.click_on('bulk upload username button')    
        crm.fill_in_textbox("123xyz", 'bulk upload search username')
        crm.click_on('bulk upload go button ')                  
        crm.click_on('bulk upload choose button')
        alert_text = crm.switch_to_alert().text 
        #print(alert_text)
        Assert.equal("Please choose username", alert_text, "Warning to user to enter correct username")        
        
        
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_listing_without_image (self, selenium, base_url):
        '''
        @summary: Upload without image and warning user to add image
        
        Steps: 
        1. Choose correct username
        2. Enter all data without image
        3. Warning user to add image for listing
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        crm.click_on('bulk upload username button')    
        crm.fill_in_textbox("aileen_dealer_my2", 'bulk upload search username')
        crm.click_on('bulk upload go button ')         
        crm.click_on('bulk upload search datausername')          
        crm.click_on('bulk upload choose button')             
        crm.click_on('bulk upload add listings button') 
        #skip image
        #expand vechicle
        crm.click_on('bulk upload expand details')                        
        crm.select_drop_down_using_visible_text("Make", "Proton")
        crm.wait_for_seconds(1)   
        crm.select_drop_down_using_visible_text("Model", "Saga")
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Year", "2015")       
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Transmission", "Automatic")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Variant", "1.3 FLX Executive")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload vehicle dropdown', 'PROT15AL')
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("CarType", "USED")        
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("AssembleType", "CKD")           
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Color", "Gold")         
        crm.wait_for_seconds(1)          
        crm.select_drop_down_using_visible_text("Location", "Kuala Lumpur")             
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Area", "Bangsar")     
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("MillageRange", "Other")    
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload millagerange dropdown', '12500')
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("abc123", 'bulk upload reference number')         
        crm.fill_in_textbox("w123x", 'bulk upload plate number') 
        #expand price
        crm.click_on('bulk upload expand details')  
        crm.fill_in_textbox("27000", 'bulk upload price')         
        #expand title & description
        crm.click_on('bulk upload expand details')
        crm.wait_for_seconds(1)          
        crm.fill_in_textbox("testing", 'bulk upload listing description')
        crm.wait_for_seconds(1)                           
        crm.click_on('bulk upload add listing button')
        crm.wait_for_seconds(1)  
        #check for missing images for listing message
        alert_text = crm.switch_to_alert().text 
        Assert.contains("Please add images for listing.", alert_text, "Warning to user to add images for listing")
        
        
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_listing_no_cartype (self, selenium, base_url):
        '''
        @summary: Upload without image and warning user to add image
        
        Steps: OK
        1. Choose correct username
        2. Enter all data without image
        3. Warning user to CarType for listing
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        crm.click_on('bulk upload username button')
        crm.wait_for_seconds(1)     
        crm.fill_in_textbox("aileen_dealer_my2", 'bulk upload search username')
        crm.click_on('bulk upload go button ')         
        crm.click_on('bulk upload search datausername')          
        crm.click_on('bulk upload choose button')             
        crm.click_on('bulk upload add listings button') 
        #Add image
        crm.click_on('bulk upload select main image')           
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\MasterUploadListing.exe")
        crm.click_on('bulk upload select secondary image') 
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUploadListing.exe")        
        #epand vehicle
        crm.click_on('bulk upload expand details')                        
        crm.select_drop_down_using_visible_text("Make", "Proton")
        crm.wait_for_seconds(1)   
        crm.select_drop_down_using_visible_text("Model", "Saga")
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Year", "2015")       
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Transmission", "Automatic")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Variant", "1.3 FLX Executive")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload vehicle dropdown', 'PROT15AL')
        crm.wait_for_seconds(1)  
        #skip CarType
        crm.select_drop_down_using_visible_text("AssembleType", "CKD")           
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Color", "Gold")         
        crm.wait_for_seconds(1)          
        crm.select_drop_down_using_visible_text("Location", "Kuala Lumpur")             
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Area", "Bangsar")     
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("MillageRange", "Other")    
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload millagerange dropdown', '12500')
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("abc123", 'bulk upload reference number')         
        crm.fill_in_textbox("w123x", 'bulk upload plate number') 
        #expand price
        crm.click_on('bulk upload expand details')  
        crm.fill_in_textbox("27000", 'bulk upload price')         
        #expand title & description
        crm.click_on('bulk upload expand details')
        crm.wait_for_seconds(1)          
        crm.fill_in_textbox("testing", 'bulk upload listing description')  
        crm.click_on('bulk upload add listing button')
        crm.wait_for_seconds(1)  
        #Check for missing car type alert
        alert_text = crm.switch_to_alert().text 
        #print(alert_text)
        Assert.contains("Please Select Car Type.", alert_text, "Warning to user to add car type")        
                 

    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_listing_no_price (self, selenium, base_url):
        '''
        @summary: Upload without price and warning user to add price
        
        Steps: Pass
        1. Choose correct username
        2. Enter all data without price
        3. Warning user to enter price for listing
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        crm.click_on('bulk upload username button')    
        crm.fill_in_textbox("aileen_dealer_my2", 'bulk upload search username')
        crm.click_on('bulk upload go button ')         
        crm.click_on('bulk upload search datausername')          
        crm.click_on('bulk upload choose button')             
        crm.click_on('bulk upload add listings button') 
        #Add image
        crm.click_on('bulk upload select main image')           
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\MasterUploadListing.exe")
        crm.click_on('bulk upload select secondary image') 
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUploadListing.exe")        
        #epand vehicle
        crm.click_on('bulk upload expand details')                        
        crm.select_drop_down_using_visible_text("Make", "Proton")
        crm.wait_for_seconds(1)   
        crm.select_drop_down_using_visible_text("Model", "Saga")
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Year", "2015")       
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Transmission", "Automatic")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Variant", "1.3 FLX Executive")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload vehicle dropdown', 'PROT15AL')
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("CarType", "USED")
        crm.wait_for_seconds(1)               
        crm.select_drop_down_using_visible_text("AssembleType", "CKD")           
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Color", "Gold")         
        crm.wait_for_seconds(1)          
        crm.select_drop_down_using_visible_text("Location", "Kuala Lumpur")             
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Area", "Bangsar")     
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("MillageRange", "Other")    
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload millagerange dropdown', '12500')
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("abc123", 'bulk upload reference number')         
        crm.fill_in_textbox("w123x", 'bulk upload plate number') 
        #expand price
        crm.click_on('bulk upload expand details')
        #skip price  
       # crm.fill_in_textbox("27000", 'bulk upload price')         
        #expand title & description
        crm.click_on('bulk upload expand details')
        crm.wait_for_seconds(1)          
        crm.fill_in_textbox("testing", 'bulk upload listing description')  
        crm.click_on('bulk upload add listing button')
        crm.wait_for_seconds(1)  
        #check price message
        alert_text = crm.switch_to_alert().text 
        #print(alert_text)
        Assert.contains("Please enter Price.", alert_text, "Warning to user to enter price")        



    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_without_username_add_listing(self, selenium, base_url):
        '''
        @summary: Upload without username and then add price,warning user to choose username or remove empty user
        
        Steps: OK
        1. Choose correct username
        2. Enter without username
        3. Add listing
        4. Prompt user to choose username or remove empty user
        5. to do
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        #skip enter username           
        crm.click_on('bulk upload add listings button') 
        alert_text = crm.switch_to_alert().text 
        #print(alert_text)
        Assert.equal("Please choose missing users or remove empty users", alert_text, "Warning to user to enter username")     



    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_progress_pages(self, selenium, base_url):
        '''
        @summary: Check bulk upload listings progress
        
        Steps:
        1. Go to bulk upload progress
        2. Click on upload bulk progress and page is display 
        3. Click on upload bulk progress details is display 
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload progress')
        crm.switch_to_new_window()
        #check the grid is display after open upload progress page
        crm.wait_for_seconds(1)
        Assert.true(crm.element_visibility('bulk upload progress grid'), 'bulk upload progress grid is show')
        #check the pagination work perfectly        
        crm.click_on('bulk upload progress pagination')
        #check the grid is display after click on pagination          
        Assert.true(crm.element_visibility('bulk upload progress grid'), 'bulk upload progress grid is show')
        crm.click_on('bulk upload progress action button')
        #check the grid is display in bulk upload progress details   
        Assert.true(crm.element_visibility('bulk upload progress grid'), 'bulk upload progress grid is show') 
        crm.click_on('bulk upload progress details back to list button')
        Assert.true(crm.element_visibility('bulk upload progress title'), 'bulk upload progress title is show')         
        
        
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_progress_search(self, selenium, base_url):
        '''
        @summary: Check bulk upload listings progress search result
        
        Steps:
        1. Go to bulk upload progress
        2. Search on upload bulk progress and result is display 
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload progress')
        crm.switch_to_new_window()
        #check the grid is display after open upload progress page
        crm.wait_for_seconds(1)
        Assert.true(crm.element_visibility('bulk upload progress grid'), 'bulk upload progress grid is show')
        crm.fill_in_textbox("aileen.ong", 'bulk upload progress search textfield')
        crm.click_on('bulk upload progress go button')
        crm.wait_for_seconds(1)        
        Assert.true(crm.element_visibility('bulk upload progress action button'), 'result is searched')



    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_upload_bulk_multiple_listing (self, selenium, base_url):
        '''
        @summary: Upload bulk upload listing
        
        Steps: pass
        1. Select correct 2 usernames
        2. Upload 10 listings (positive test)
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk upload tool treeview')
        crm.click_on('bulk upload listings')
        crm.switch_to_new_window()
        #add first username
        crm.click_on('bulk upload username button')    
        crm.fill_in_textbox("aileen_dealer_my2", 'bulk upload search username')
        crm.click_on('bulk upload go button ')         
        crm.click_on('bulk upload search datausername')          
        crm.click_on('bulk upload choose button')
        #add second username
        crm.click_on('bulk upload add username button')
        crm.click_on('bulk upload username2 button')   
        crm.fill_in_textbox("aileen_dealer_my3", 'bulk upload search username')
        crm.click_on('bulk upload go button ')         
        crm.click_on('bulk upload search datausername')          
        crm.click_on('bulk upload choose button')        
        #Add first listing
        crm.click_on('bulk upload add listings button') 
        #Add image
        crm.click_on('bulk upload select main image')           
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\BulkUploadListing.exe")
        crm.click_on('bulk upload select secondary image') 
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUploadListing.exe")        
        #expand vehicle
        crm.click_on('bulk upload expand details')                        
        crm.select_drop_down_using_visible_text("Make", "Proton")
        crm.wait_for_seconds(1)   
        crm.select_drop_down_using_visible_text("Model", "Saga")
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Year", "2015")       
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Transmission", "Automatic")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Variant", "1.3 FLX Executive")               
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload vehicle dropdown', 'PROT15AL')
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("CarType", "USED")        
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("AssembleType", "CKD")           
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Color", "Gold")         
        crm.wait_for_seconds(1)          
        crm.select_drop_down_using_visible_text("Location", "Kuala Lumpur")             
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("Area", "Bangsar")     
        crm.wait_for_seconds(1)  
        crm.select_drop_down_using_visible_text("MillageRange", "Other")    
        crm.wait_for_seconds(1)  
        crm.select_drop_down_by_value('bulk upload millagerange dropdown', '12500')
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("abc123", 'bulk upload reference number')         
        crm.fill_in_textbox("w123x", 'bulk upload plate number') 
        #expand price
        crm.click_on('bulk upload expand details')  
        crm.fill_in_textbox("27000", 'bulk upload price')         
        #expand title & description
        crm.click_on('bulk upload expand details')
        crm.wait_for_seconds(1)          
        crm.fill_in_textbox("testing", 'bulk upload listing description')
        crm.click_on('bulk upload select all static')
        crm.wait_for_seconds(1)      
        crm.click_on('bulk upload add listing button')
        for addlistings in range(9):
        #Add 2 -9 listings
            crm.click_on('bulk upload add listings button') 
            #Add image
            crm.click_on('bulk upload select main image')           
            subprocess.call("E:\git\icarasia_sp_automation\AutoIT\BulkUploadListing.exe")
            crm.click_on('bulk upload select secondary image') 
            subprocess.call("E:\git\icarasia_sp_automation\AutoIT\FileUploadListing.exe")        
            #epand vehicle
            #crm.click_on('bulk upload expand details')                        
            crm.select_drop_down_using_visible_text("Make", "Proton")
            crm.wait_for_seconds(1)   
            crm.select_drop_down_using_visible_text("Model", "Saga")
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("Year", "2015")       
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("Transmission", "Automatic")               
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("Variant", "1.3 FLX Executive")               
            crm.wait_for_seconds(1)  
            crm.select_drop_down_by_value('bulk upload vehicle dropdown', 'PROT15AL')
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("CarType", "USED")        
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("AssembleType", "CKD")           
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("Color", "Gold")         
            crm.wait_for_seconds(1)          
            crm.select_drop_down_using_visible_text("Location", "Kuala Lumpur")             
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("Area", "Bangsar")     
            crm.wait_for_seconds(1)  
            crm.select_drop_down_using_visible_text("MillageRange", "Other")    
            crm.wait_for_seconds(1)  
            crm.select_drop_down_by_value('bulk upload millagerange dropdown', '12500')
            crm.wait_for_seconds(1)  
            crm.fill_in_textbox("abc123", 'bulk upload reference number')         
            crm.fill_in_textbox("w123x", 'bulk upload plate number') 
            #expand price
            #crm.click_on('bulk upload expand details')  
            crm.fill_in_textbox("27000", 'bulk upload price')         
            #expand title & description
            #crm.click_on('bulk upload expand details')
            crm.wait_for_seconds(1)          
            crm.fill_in_textbox("testing", 'bulk upload listing description')
            crm.click_on('bulk upload select all static')
            crm.wait_for_seconds(1)      
            crm.click_on('bulk upload add listing button')              
        crm.wait_for_seconds(1)  
        crm.fill_in_textbox("test bulk upload tool", 'bulk upload add common description')
        crm.wait_for_seconds(1)              
        #successful upload listing           
        crm.click_on('bulk upload upload button')             
        Assert.true(crm.element_visibility('bulk upload success message'), 'Successfully upload listing.')





