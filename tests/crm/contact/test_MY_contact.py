import subprocess
import urllib2
import pytest
from unittestzero import Assert

from locators.locators_crm import Crm_locators
from pages.common_page import Common_Page


    
class TestCRM_ContactMY():

    
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_search_by_username (self, selenium, base_url):
        '''
        @summary: Search by username (positive test)
        
        Steps:
        1. Go to contact
        2. Search by username
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')           
        crm.fill_in_textbox("aileen_dealer_my2", 'contact username input')            
        crm.click_on('contact search button')
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_search_by_email(self, selenium, base_url):
        '''
        @summary: Search by email (positive test)
        
        Steps:
        1. Go to contact
        2. Search by email
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        crm.click_on('customer treeview')
        crm.click_on('contact')          
        crm.fill_in_textbox("aileen.ong@icarasia.com", 'contact email input')            
        crm.click_on('contact search button')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_search_by_phonenumber(self, selenium, base_url):
        '''
        @summary: Search by email (positive test)
        
        Steps:
        1. Go to contact
        2. Search by phone number
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')    
        crm.fill_in_textbox("0169282863", 'contact phone number input')            
        crm.click_on('contact search button')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')


    @pytest.mark.nondestructive
    @pytest.mark.AL
    def test_contact_search_by_fullname(self, selenium, base_url):
        '''
        @summary: Search by phone number (positive test)
        
        Steps:
        1. Go to contact
        2. Search by phone number
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')          
        crm.fill_in_textbox("New Car Deal Carlist", 'contact full name input')            
        crm.click_on('contact search button')
        while crm.is_loading():
            crm.wait_for_seconds(1)      
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_details_from_hyperlink(self, selenium, base_url):
        '''
        @summary: Search by phone number (positive test)
        
        Steps:
        1. Go to contact
        2. Search by username
        3. Result searched 
        4. Click on the hyperlink of username
        5. Contact details page is displayed
        
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')          
        crm.fill_in_textbox("aileen_dealer_my2", 'contact username input')         
        crm.click_on('contact search button')
        crm.wait_for_seconds(1)
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')  
        #click on username details  
        crm.click_on('contact result table')
        crm.switch_to_new_window()    
        Assert.true(crm.element_visibility('contact result details'), 'result details is not correct')    

        

    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_details_from_action(self, selenium, base_url):
        '''
        @summary: Search by phone number (positive test)
        
        Steps:
        1. Go to contact
        2. Search by username
        3. Result searched 
        4. Click on the action of username
        5. Contact details page is displayed
        
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')          
        crm.fill_in_textbox("aileen_dealer_my2", 'contact username input')         
        crm.click_on('contact search button')
        crm.wait_for_seconds(1) 
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')  
        #click on username details       
        crm.click_on('contact result action details')
        crm.switch_to_new_window()
        Assert.true(crm.element_visibility('contact result details'), 'result details is not correct')   
               

    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_edit_from_action(self, selenium, base_url):
        '''
        @summary: Search by phone number (positive test)
        
        Steps:
        1. Go to contact
        2. Search by username
        3. Result searched 
        4. Click on the action of username
        5. Contact details page is displayed
        
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')          
        crm.fill_in_textbox("aileen_dealer_my2", 'contact username input')         
        crm.click_on('contact search button')
        crm.wait_for_seconds(1) 
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')  
        #click on username details       
        crm.click_on('contact result action edit')
        crm.switch_to_new_window()
        crm.wait_for_seconds(1) 
        #something wrong with the clear textbox
        crm.clear_textbox('contact edit email textbox')
        crm.fill_in_textbox("aileen.ong@icarasia.com", 'contact edit email textbox')               
        crm.clear_textbox('contact edit mobile phone textbox')   
        crm.fill_in_textbox("0122114929", 'contact edit mobile phone textbox')      
        crm.click_on('contact save button')

    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_contact_sms_verfication(self, selenium, base_url):
        '''
        @summary: Search by phone number (positive test)
        
        Steps:
        1. Go to contact
        2. Search by username
        3. Result searched 
        4. Click on the hyperlink of username
        5. Contact details page is displayed
        
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('aileen.ong')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('customer treeview')
        crm.click_on('contact')          
        crm.fill_in_textbox("aileen_dealer_my2", 'contact username input')         
        crm.click_on('contact search button')
        crm.wait_for_seconds(1)
        Assert.true(crm.element_visibility('contact result table'), 'result is searched')  
        #click on username details  
        crm.click_on('contact result table')
        crm.switch_to_new_window()    
        Assert.true(crm.element_visibility('contact result details'), 'result details is not correct')   
        crm.click_on('contact account management tab')
