import subprocess
import pytest
from unittestzero import Assert

from locators.locators_crm import Crm_locators
from pages.common_page import Common_Page

    
class TestCRM_TH():

    
    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_remove_bulk_listing (self, selenium, base_url):
        '''
        @summary: remove bulk listing (positive test)
        
        Steps: pass
        1. Select a correct csv file
        2. Add published listings to change to Archieved
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('QAUSER.TH')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk removal tool treeview')
        crm.click_on('bulk removal remove listings')
        crm.switch_to_new_window()
        crm.click_on('bulk removal browse file')        
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\RemoveListings.exe")        
        crm.fill_in_textbox("testing removal bulk listing from QA", 'bulk removal description')         
        crm.click_on('bulk removal upload button')        
        Assert.true(crm.element_visibility('bulk removal success message'), 'Successfully remove listing.')


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_remove_bulk_no_upload_file (self, selenium, base_url):
        '''
        @summary: remove bulk listing (negative test)
        
        Steps: pass
        1. No upload csv file
        2. Click upload button
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('QAUSER.TH')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk removal tool treeview')
        crm.click_on('bulk removal remove listings')
        crm.switch_to_new_window()      
        crm.fill_in_textbox("testing removal bulk listing from QA", 'bulk removal description')         
        crm.click_on('bulk removal upload button')
        alert_text = crm.switch_to_alert().text                 
        Assert.contains("Please Upload the File for this request", alert_text, "Warning to user to enter price")     


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_remove_bulk_upload_incorrect_file (self, selenium, base_url):
        '''
        @summary: remove bulk listing (negative test)
        
        Steps: pass
        1. Upload incorrect .jpg file
        2. Click upload button
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('QAUSER.TH')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk removal tool treeview')
        crm.click_on('bulk removal remove listings')
        crm.switch_to_new_window()      
        crm.click_on('bulk removal browse file')        
        subprocess.call("E:\git\icarasia_sp_automation\AutoIT\MasterUploadListing.exe")        
        crm.fill_in_textbox("testing removal bulk listing from QA", 'bulk removal description')         
        crm.click_on('bulk removal upload button')        
        Assert.true(crm.element_visibility('bulk removal failed message'), 'Failed to remove listing.')  
        


    @pytest.mark.nondestructive
    @pytest.mark.id
    def test_bulk_removal_progress (self, selenium, base_url):
        '''
        @summary: Check removal progress (positive test)
        
        Steps: pass
        1. Check removal progress page
        2. Ensure all the page can be accessed
        '''
        
        crmUrl = 'http://crm.preprod.carlist.my:90/Account/login'
        locator = Crm_locators(crmUrl)
        crm = Common_Page(selenium, crmUrl, locator)
        crm.go_to_home_page()
        #crm.click_on('sign in link')
        crm.login_with_crm_user('QAUSER.TH')
        while crm.is_loading():
            crm.wait_for_seconds(1)   
        crm.click_on('bulk removal tool treeview')
        crm.click_on('bulk removal progress')
        crm.switch_to_new_window()
        Assert.true(crm.element_visibility('bulk removal progress title'), 'Bulk removal progress cannot access.')      
        crm.click_on('bulk upload progress action button')
        Assert.true(crm.element_visibility('bulk removal request details title'), 'Removal request details cannot access.')          
        crm.click_on('bulk removal request details back button')        
        Assert.true(crm.element_visibility('bulk removal progress title'), 'Bulk removal progress cannot access.')        
       