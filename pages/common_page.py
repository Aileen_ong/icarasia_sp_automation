'''Inventory related'''
from pages.common import Common
from locators.locators_common import locators
from selenium.webdriver.support.select import Select
import random
import re
from gc import get_objects

class Common_Page(Common):
    '''Functions for inventory test'''
   
    def __init__(self, selenium, url, customLocator):
        '''__init__'''
        self.selenium = selenium
        self.url = url
        self.customLocator = customLocator

    def get_object(self, locator_key, num=None):
        '''get locator object'''
        #elem = self.customLocator.get_locator(element)

        return self.customLocator.get_locator(locator_key, num)
    
    def click_on(self, locator_key, num=None):
        '''Click on element'''
        obj_to_click = self.get_object(locator_key, num)
        
        self.click_on_element(obj_to_click)
        
    def hover(self, locator_key):
        '''wd = webdriver_connection.connection
        'element = wd.find_element_by_link_text(self.locator)
        'hov = ActionChains(wd).move_to_element(element)
        'hov.perform()'''
        obj_to_hover = self.get_object(locator_key)
        self.hover_on_element(obj_to_hover)
        
    def expected_page_title(self):
        '''return expected page title for comparison'''
        # return False if error page detected
        if self.is_error_page():
            return 'Error page is found!'
        
        if re.search('carlist.my', self.url, re.IGNORECASE):
            if re.search('dealer', self.get_current_page_url(), re.IGNORECASE):
                if re.search('privateseller', self.get_current_page_url(), re.IGNORECASE):
                    exp_title = 'Carlist.my'
                else:
                    exp_title = 'Response Management System'
            else:
                exp_title = 'Carlist.my'
        elif re.search('mobil123.com', self.url, re.IGNORECASE):
            if re.search('dealer', self.get_current_page_url(), re.IGNORECASE):
                if re.search('privateseller', self.get_current_page_url(), re.IGNORECASE):
                    exp_title = 'Mobil123.com'
                else:
                    exp_title = 'Si Jari'
            else:
                exp_title = 'Mobil123.com'
        elif re.search('one2car.com', self.url, re.IGNORECASE):
            exp_title = 'One2car.com'
        
        # expected string should found
        return exp_title
    
    def count_for_section_element(self, locator):
        '''return count for element'''
        locator = locator.strip().lower()
        
        elem = self.get_object(locator)
        
        return self.get_number_of_nodes(elem)

    def element_visibility(self, locator_key):
        obj = self.get_object(locator_key)
        
        return self.is_element_visible(obj)

    def is_error_page(self):
        'return if current page is error page'
        err_page = self.is_element_present(self.get_object('error page'))

        return err_page

    def clear_value_in_textbox(self, locator_key):
        '''clear the value in the text box'''
        elem = self.customLocator.get_object(locator_key)
        
        self.clear_textbox(elem)


    def get_number_of_options_in_drop_down(self, locator, skip_wait=True):
        '''return number of options value in a drop down'''
        num = self.get_num_of_elements(locator, skip_wait=skip_wait)
        
        return num
    
    def get_num_of_elements(self, element, skip_wait=True):
        '''Calling common based class function to get number of elements by xpath'''
        element_num = self.get_number_of_nodes(self.get_object(element), skip_wait=skip_wait)
        
        return element_num
    

    def fill_in_textbox(self, string, locator_key):
        '''fill the text box by giving string'''
        elem = self.get_object(locator_key)
        
        self.fill_textbox(elem, string)
        

    def is_drop_down_contains_options(self, element):
        '''Get drop down contains options value list'''
        has_options = self.get_element_attribute(self.get_object(element), 'class')
        
        return re.search('has-options', has_options, re.IGNORECASE)    
    
    def select_drop_down_by_index(self, locator_key, index):
        xpath = self.get_object(locator_key)
        self.select_drop_down_byIndex(xpath, index)  

    def select_drop_down_by_value(self, locator_key, value):
        xpath = self.get_object(locator_key)
        self.select_drop_down_byValue(xpath, value) 
        

        
    def login_with_crm_user(self, username, password='iCar2016@'):
        '''Login into crm portal'''
        username = username.strip()
        password = password.strip()
        
        self.fill_textbox(self.get_object('crm username textbox'), username)
        self.fill_textbox(self.get_object('crm password textbox'), password)
        self.click_on('crm login button') 


    def clear_textbox(self, element):
        '''Clear the value in the text box'''
        self.wait_for_element(element)
        self.selenium.find_element_by_xpath(element).clear()
