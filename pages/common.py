'''All common functions'''
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.webdriver.support.select import Select
import re
import os

class Common(object):
    '''Base class for global functions'''
    
    def go_to_page(self, url):
        '''Go to desired url'''
        try:
            self.selenium.get(url)
        except TimeoutException:
            self.execute_js('window.stop();')
        
    def wait_for_element(self, element):
        '''Selenium implicit wait for 60 seconds until element is present'''
        try:
            WebDriverWait(self.selenium, 60).until(lambda driver: self.selenium.find_element_by_xpath(element))
        except TimeoutException:
            raise Exception('Unable to locate element: %s' % (element))

    def get_page_title(self):
        '''Get current page title'''
        page_title = self.selenium.title
        
        return page_title.strip()
    
    def save_auth_info_to_cookies(self):
        '''A hack to authenticate all site first before start running test'''
        bp_auth = 'icar:6gears'
        dp_auth = 'uatuser:iCarAsia999'
        url = self.url
        buyerp_url = ''
        dealerp_url = ''
        
        if re.search('preprod', url, re.IGNORECASE):
            env = 'preprod'
        elif re.search('staging', url, re.IGNORECASE):
            env = 'staging'
        else:
            env = None
        
        # get server env: carlist, mobil123, one2car
        server_path = re.split('https://', url)
        server_path = re.split('\.', server_path[-1])
        server_path_split = (server_path[-2], server_path[-1])

        server_url = '.'
        server_url = server_url.join(server_path_split)

        # Form the complete URL for BP and DP
        if env != None:
            #buyerp_url = 'https://%s@%s.%s' % (bp_auth, env, server_url)
            dealerp_url = 'https://%s@dealer.%s.%s' % (dp_auth, env, server_url)
        
        else:
           # buyerp_url = 'https://www.%s' % (server_url)            
            dealerp_url = 'https://dealer.%s' % (server_url)        
        
        for portal_url in [dealerp_url, buyerp_url]:
            print 'hello'
            print ('portal_url',  portal_url)
            #print (portal_url)
            #print (dealerp_url)
            if portal_url == '':
                continue
            
            #print (portal_url)
            print "hello2"
            self.go_to_page(portal_url)
            self.wait_for_seconds(1)
                
            
      
    def go_to_home_page(self, save_auth=True):
        '''Navigate to home page'''
        self.maximise_window()
        if save_auth:
            self.save_auth_info_to_cookies()
        #if not self.url == self.get_current_page_url().strip("/"):
        print 'url: ' + str(self.url)
        self.go_to_page(self.url)

    def maximise_window(self):
        '''Maximize browser window'''
        self.selenium.maximize_window()
    
    def get_current_page_url(self):
        '''Return current page url'''
        url = self.selenium.current_url
        
        return url

    def refresh_current_page(self):
        '''Refresh current page'''
        try:
            self.selenium.refresh()
        except TimeoutException:
            self.execute_js('window.stop();')
    
    def click_on_element(self, element, skip_wait=False):
        '''Click on element on the page'''
        if not skip_wait:
            self.wait_for_element(element)
        
        action = ActionChains(self.selenium)
        action.move_to_element(self.selenium.find_element_by_xpath(element))
        action.click()
        action.perform()
        
        self.wait_for_seconds(1)

    def hover_on_element(self, element):
        '''wd = webdriver_connection.connection
        'element = wd.find_element_by_link_text(self.locator)
        'hov = ActionChains(wd).move_to_element(element)
        'hov.perform()'''
        action = ActionChains(self.selenium)
        hov = action.move_to_element(self.selenium.find_element_by_xpath(element)) 
        hov.perform()
        
    def dropdown_on_element(self, element):
        '''mySelect = Select(driver.find_element_by_id("mySelectID"))
        mySelect.select_by_value("Value")'''
        select = Select(self.selenium.find_element_by_xpath(element))
        select.select_by_visible_text()



    def get_text_element(self, element):
        '''Get text from an element'''
        self.wait_for_element(element)
        text = self.selenium.find_element_by_xpath(element).text
        
        return text.strip()

    def get_radio_button_seleccted(self, element):
        '''Get the checked status for radio button'''
        self.wait_for_element(element)
        
        checked = self.selenium.find_element_by_xpath(element).is_selected()
        
        return checked
        
    def execute_js(self, js_script):
        '''Execute javascript by selenium'''
        self.selenium.execute_script(js_script)
        
    def scroll_to_top_of_the_page(self):
        '''scroll to top of the page'''
        self.execute_js('window.scrollTo(0, 0);')
        self.wait_for_seconds(1)
        
    def scroll_to_bottom_of_the_page(self):
        '''scroll to bottom of the page'''
        self.execute_js('window.scrollTo(0, document.body.scrollHeight);')
        self.wait_for_seconds(1)
    
    def wait_for_seconds(self, time):
        '''Explicit wait for execution'''
        default_timeout_1s = 1
        default_timeout_2s = 2

        if time == 1:
            sleep(default_timeout_1s)
        if time == 2:
            sleep(default_timeout_2s)
        else:
            # max sleep 3 seconds
            sleep(default_timeout_2s)
            sleep(default_timeout_1s)

    def go_to_previous_page(self):
        '''Cliking on browser back button'''
        self.selenium.back()
        self.wait_for_seconds(1)

    def get_current_window_handler(self):
        '''Return current active window handler'''
        window_handles = self.selenium.current_window_handle
        
        return window_handles
        
    def switch_to_new_window(self, window=None):
        '''Switch to new opened window'''
        if window == None:
            window_handles = self.selenium.window_handles
            
            if len(window_handles) <= 1:
                raise Exception('No new window found!')
            
            self.selenium.switch_to_window(window_handles[1])
        else:
            self.selenium.switch_to_window(window)

    def switch_to_alert(self):
        return self.selenium.switch_to_alert()

   

    def login_with_user(self, username, password='jobjob123'):
        '''Login into buyer portal'''
        username = username.strip()
        password = password.strip()
        
        self.fill_textbox(self.get_object('username textbox'), username)
        self.fill_textbox(self.get_object('password textbox'), password)
        self.hide_homepage_header()
        self.click_on('sign in button')
        
        
    def close_window(self):
        '''Close the current window tab'''
        self.selenium.close()

    def get_element_attribute(self, element, attribute):
        '''Get attribute of element'''
        self.wait_for_element(element)
        value = self.selenium.find_element_by_xpath(element).get_attribute(attribute)
        
        return value.strip()

    def fill_textbox(self, element, string):
        '''Fill the text box by giving string'''
        self.wait_for_element(element)
        
        if string == 'ESC':
            self.selenium.find_element_by_xpath(element).send_keys(Keys.ESCAPE)
        else:
            self.selenium.find_element_by_xpath(element).send_keys(string)

    def clear_textbox(self, element):
        '''Clear the value in the text box'''
        self.wait_for_element(element)
        self.selenium.find_element_by_xpath(element).clear()


    def get_number_of_nodes(self, element, skip_wait=True):
        '''Return count for elements'''
        if skip_wait:
            self.wait_for_element(element)
        num = self.selenium.find_elements_by_xpath(element)
        
        return len(num)

    def hide_bottom_ads(self):
        '''Hide the ads appear on bottom of the page'''
        # execute jquery
        path = r'pages\jquery\jquery-1.12.4.min.js'
        jq_path = os.path.join(os.getcwd(), path)
        
        jquery = open(jq_path, 'r')
        jquery = jquery.read()
        
        self.execute_js(jquery)
        
        # hide the bottom ads
        self.execute_js('$("div[id^=\'ebAd36763077_banner_div\']").hide()')

    def hide_header(self):
        '''Hack for FF to hide sticky header when page scroll to lower position'''
        js_script = 'document.getElementsByClassName("js-header--sticky-top")[0].style.visibility = "hidden";'
        #js_script='document.getElementById("main-topbar").style.visibility = "hidden";'
        self.execute_js(js_script)

    def hide_homepage_header(self):
        '''Hack for FF to hide sticky header when page scroll to lower position'''
        #js_script = 'document.getElementsByClassName("js-header--sticky-top")[0].style.visibility = "hidden";'
        js_script='document.getElementById("main-topbar").style.visibility = "hidden";'
        self.execute_js(js_script)

    def hide_version_update_header(self):
        '''Hack for FF to hide sticky header when page scroll to lower position'''
        #js_script = 'document.getElementsByClassName("js-header--sticky-top")[0].style.visibility = "hidden";'
        js_script='document.getElementByXpath("//*[@id="buorg"]").style.visibility = "hidden";'
        self.execute_js(js_script)


    def unhide_header(self):
        '''Display the page header again after get hidden'''
        js_script = 'document.getElementsByClassName("js-header--sticky-top")[0].style.visibility = "visible";'
        self.execute_js(js_script)
    
    def is_loading(self):
        '''return true is loading icon is visible on the listing page'''
        return self.is_element_visible(self.get_object('loading icon'))
    
    def is_element_visible(self, locator):
        '''Return true if element is visible on page'''
        attempt = 0
        while attempt < 3:
            try:
                return self.selenium.find_element_by_xpath(locator).is_displayed()
            except NoSuchElementException:
                return False
            except ElementNotVisibleException:
                return False
            except StaleElementReferenceException:
                pass
            attempt = attempt + 1

    def is_element_present(self, locator):
        '''Return true if element exist inside page, no matter it is visible'''
        # self.selenium.implicitly_wait(0)
        try:
            self.selenium.find_element_by_xpath(locator)
            return True
        except NoSuchElementException:
            return False
        finally:
            # set back to where you once belonged
            pass
        
        
    def fill_in_textbox(self, string, element):
        '''fill the text box by giving string'''
        elem = self.get_object(element)
        
        self.fill_textbox(elem, string)        

    def get_number_of_options_in_drop_down(self, locator, skip_wait=True):
        '''return number of options value in a drop down'''
        num = self.get_num_of_elements(locator, skip_wait=skip_wait)
        
        return num
    
    def get_num_of_elements(self, element, skip_wait=True):
        '''Calling common based class function to get number of elements by xpath'''
        element_num = self.get_number_of_nodes(self.get_object(element), skip_wait=skip_wait)
        
        return element_num
    
    #select by value
    def select_drop_down_byValue(self, xpath, value):
        element = self.selenium.find_element_by_xpath(xpath)
        selectElement = Select(element)
        selectElement.select_by_value(value)
    
    # Select DropDown by Index
    def select_drop_down_byIndex(self, xpath, index):
        element = self.selenium.find_element_by_xpath(xpath)
        
        selectElement = Select(element)
        selectElement.select_by_index(index)


    def select_drop_down_using_visible_text(self, elementId, visibleText):
        element = self.selenium.find_element_by_id(elementId)
        # selected by visible text
        select = Select(element)
        select.select_by_visible_text(visibleText)
        



