'''Home page related'''
from pages.common import Common
from locators.locators_home_page import Home_Page_locators
import re

class Home_Page(Common):
    '''Functions for Home page test'''
    
    def __init__(self, selenium, url):
        '''__init__'''
        self.selenium = selenium
        self.url = url

    def get_object(self, element):
        '''get locator object'''
        elem = Home_Page_locators(self.url)

        return elem.get_locator(element)
    
    def click_on(self, element):
        '''Click on element'''
        obj_to_click = self.get_object(element)
        
        self.click_on_element(obj_to_click)
        
    def hover(self, element):
        '''wd = webdriver_connection.connection
        'element = wd.find_element_by_link_text(self.locator)
        'hov = ActionChains(wd).move_to_element(element)
        'hov.perform()'''
        obj_to_hover = self.get_object(element)
        self.hover_on_element(obj_to_hover)
        
    def expected_page_title(self):
        '''return expected page title for comparison'''
        # return False if error page detected
        if self.is_error_page():
            return 'Error page is found!'
        
        if re.search('carlist.my', self.url, re.IGNORECASE):
            if re.search('dealer', self.get_current_page_url(), re.IGNORECASE):
                if re.search('privateseller', self.get_current_page_url(), re.IGNORECASE):
                    exp_title = 'Carlist.my'
                else:
                    exp_title = 'Response Management System'
            else:
                exp_title = 'Carlist.my'
        elif re.search('mobil123.com', self.url, re.IGNORECASE):
            if re.search('dealer', self.get_current_page_url(), re.IGNORECASE):
                if re.search('privateseller', self.get_current_page_url(), re.IGNORECASE):
                    exp_title = 'Mobil123.com'
                else:
                    exp_title = 'Si Jari'
            else:
                exp_title = 'Mobil123.com'
        elif re.search('one2car.com', self.url, re.IGNORECASE):
            exp_title = 'One2car.com'
        
        # expected string should found
        return exp_title
    
    def count_for_section_element(self, locator):
        '''return count for element'''
        locator = locator.strip().lower()
        
        elem = self.get_object(locator)
        
        return self.get_number_of_nodes(elem)

    def element_visibility(self, element):
        obj = self.get_object(element)
        
        return self.is_element_visible(obj)

    def is_error_page(self):
        'return if current page is error page'
        err_page = self.is_element_present(self.get_object('error page'))

        return err_page

    def clear_value_in_textbox(self, element):
        '''clear the value in the text box'''
        elem = self.get_object(element)
        
        self.clear_textbox(elem)


    def fill_in_textbox(self, string, element):
        '''fill the text box by giving string'''
        elem = self.get_object(element)
        
        self.fill_textbox(elem, string) 
        
    
