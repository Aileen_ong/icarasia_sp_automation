# This is for **SP Test Automation code**

## This project will use _pytest-selenium_, detail check on [this](http://pytest-selenium.readthedocs.org/en/latest/user_guide.html): 
http://pytest-selenium.readthedocs.org/en/latest/user_guide.html

*Quick Run*
You will need software packages below:
- selenium
> pip install selenium
- pytest-selenium
> pip install pytest-selenium
- pytest-xdist
> pip install pytest-xdist
- install AutoIt for upload picture

Command to run test scripts:
> py.test --driver Remote --capability browserName firefox --html=Results\report.html