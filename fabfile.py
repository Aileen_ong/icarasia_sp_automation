# -*- coding: utf-8 -*-
from fabric.api import env, cd, get, local, put, run
import os

# remote server credential
env.user = 'cyg_server'
env.password = 'Qwerty123'
env.hosts = ['10.2.200.202']
# env.hosts = ['52.74.7.154']
env.warn_only = True


def create_setup_file(base_url):
    '''create setup.cfg before testing start'''
    setup_file = 'setup.cfg'
    
    if os.path.exists(setup_file):
        os.remove(setup_file) 
    
    # create file IO object
    with open(setup_file, 'w') as f:
        # test setup settings
        pytest_plugin_string = '[tool:pytest]'
        base_url_string = 'base_url = %s' % (base_url)
        selenium_captupre_string = 'selenium_capture_debug = failure'

        list_of_strings = [pytest_plugin_string,
                           base_url_string,
                           selenium_captupre_string]

        # write into file
        for string in list_of_strings:
            f.write(string + '\n')

def test_runner(mobile_desktop='desktop', browser='firefox', country='my', concurrent_browser=5, base_url='http://www.preprod.carlist.my'):
    '''
    trigger automation test on test automation machine
    
    @param browser: browser to test
    @param country: pytest tag to run test
    @param concurrent_browser: the number of concurrent browser, max is 5
    @param base_url: BP portal url to start test
    '''
    # sanitise input
    mobile_desktop = mobile_desktop.strip().lower()
    browser = browser.strip().lower()
    country = country.strip().lower()

    # create file setup.cfg
    create_setup_file(base_url)
    
    # zip the source folder
    local('zip -r SourceCode.zip locators/* pages/* tests/* setup.cfg')

    # make unique test folder
    test_folder = 'BP_%s_%s_%s' % (mobile_desktop, country, browser)

    # remove old script on remote machine
    run('rm -rf %s' % (test_folder,))
    run('mkdir -p %s' % (test_folder,))
    
    # enter test folder
    with cd(test_folder):
        put('SourceCode.zip', 'SourceCode.zip')
        # unzip source code
        run('unzip SourceCode.zip')
        
        # trigger test
        run('py.test --driver Remote --capability browserName %s -m "%s" -n %s \
        --self-contained-html --junit-xml=Results/report.xml --html=Results/report.html' % (browser, country, concurrent_browser))

        # zip the results
        run('zip -r Report.zip Results/*')

    # delete Results folder before download results
    local('rm -rf Results')
    local('rm -rf *.*ml')
    local('rm -rf *.zip')
    get('%s/Report.zip' % (test_folder,), '.')

    # Navigate to report, try to post result on Bamboo
    local('unzip Report.zip')
